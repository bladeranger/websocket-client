# websocket-client

The original source code is [Here](https://github.com/websocket-client/websocket-client)

We add socket time out to the run_forever function [PB-711]

websocket-client module  is WebSocket client for python. This provide the low level APIs for WebSocket. All APIs are the synchronous functions.

websocket-client supports only hybi-13.

# BladeRanger Changes
* websocket/_app.py: Add sock_timeout param to the run_forever method,
 allowing customize the socket blocking calls timeout.
 [Line 183](https://bitbucket.org/bladeranger/websocket-client/src/master/websocket/_app.py#lines-183)
 & [Lines 248-250](https://bitbucket.org/bladeranger/websocket-client/src/master/websocket/_app.py#lines-248)

#License


 - BSD

# Full Readme
Original REAMDE is [HERE](https://github.com/websocket-client/websocket-client/blob/master/README.rst)